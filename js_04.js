/* Наследование
если функция внутри обьекта то это метод
 функция может быть контруктуром(функция которая производит обьект)
Если вызвать функция со словом new то это будует вызов конструктора:
Внутри функции создается новый обьект, который потом смотрит на прототип.
*/


function Human(name) {
    this.name = name;
}; // если конструктор лучше писать с большой буквы

Human.prototype.say = function(what){
  console.log(this.name + " : " + what);
};

var alex = new Human("Alex");
/* если нету return, то она возвращает новый обьект с прототипом как у себя;
 алекс наследует не от Humanа, а от прототипа (Human.prototype)
*/

// **************************************

function Human(name) {
  this.name = name;
}

Human.prototype.say = function(what) {
  console.log(this.name + " : " + what);
}

var alex = new Human("Alex");

Human('Galex'); /* this не будет указывать на новосозданный обьект,
так как обьект не создается, потому что вызивается как функция и просто испольняется
строчка за строчкой.
Появилось в глобальном обьекте новое свойство name, грубо говоря глобальная переменная */

function Human(name) {
  if (! (this instatnceof Human)) {
    return new Human(name);
  }
  this.name = name;
}

Human("Galex");
//  get object: { name: "Galex" }

// ********************************************************

var alex = new Human ("Alex");
var jack = new Human ("Jack");

// вызвать функцию и подменить контекст
alex.say.apply(jack, ["Hi"]); // this меняется на jack и второй аргумент параметры

function speaksTo(someone) {
  console.log( this.name + " speaks to " + someone.name);
}

speaksTo(alex);
//Galex speaks to Alex - Galex - потому что содавали глобальный обьект, undefined если бы не создавали

speaksTo.apply(alex, [jack])
// Alex speaks to Jack

// *****************************************************************

function object(o) {
  function F() {}
  F.prototype = o;
  return new F(); // взвращает обьект прототипа F
}

// ******************************************************************

var parent = { name : "Alex"};
var child = Object.create(parent); // практически как и механизм выще
child
// {}
child.name
// "Alex"
