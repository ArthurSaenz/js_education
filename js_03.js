/* Функция как обьект */
function getIt() {
	return this.x;
};

var obj1 = {
	get : getIt,
	x : 1
};
var obj2 = {
	get : getIt,
	x : 2
};

obj1.get(); // 1
obj2.get(); // 2

/* функция как свойство обьекта */
var obj = {
	base : 13;
	average : function (x, y) {
		return (this.base+x+y)/3; // указывает на обьект в момент вызова
	};
};

/* аргументы и параметры */
var average1 = function() {
	var i = 0;
	var sum = 0;
	for (i=0; i < arguments.length; i++) {
		sum = sum + arguments[i];
	};
	return sum/arguments.length;
};

/*
область видимости!!!!!!!!!!!!!!!!!
во многих языках - блочная {}
в js - функциональная
(только внутри функции ограничена)
*/

var a = 10;
(function() {
	console.log(a);
})(); // сразу создал и сразу вызвал


(function() {
	console.log(a);
	var a = 11;
})()  // undefined (причина variable hoisting, ниже!!!)


(function() {
	var a;
	console.log(a);
	a = 11;
})()



/* Замыкание (closures) - функция которая имеет доступ к  свободным переменним которые были в момент вызова доступны ей  */
// нецелесообразно создавать переменную внутри функции, повлияет на производительность:
var getAnswer1 = (function(){
	var answer1 = 41;
	return answer1
});
getAnswer1(); // 41

// реализация замыкание:

var getAnswer = (function() {
	var answer = 42;

	return function inner(){
		return answer; // возвращает переменную которая уже не существует
	};
}())

getAnswer(); // 42

function greeting(name) {
	var text = "hello " + name;
	var greet = function() {
		console.log(text); // замыкание - возвращает переменную которая не в этой функции
	};
	return greet
};
a1 = greeting('Art'); // [Function]
a1(); // 'Hello Art'
