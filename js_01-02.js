function foo() {
	return {
		foo: "bar"
	}
}

1 == 1; // true
"foo" == "foo"; // true
[1, 2, 3] == [1, 2, 3]; // false (массив это указатель, разные сущности)

new Array(3) == ",,"; // true
new Array(3).toString(); // ',,'

new Array(3) === ",,"; // false

var example = "string";
example.length; // 8
example.charAt(2); //'a'

var a = 42;
a.toString(); // '42'
a.toString().length; // 2
a.toString().length.toString() // '2'

/*
Обьекты - контейнер со свойствами, но не класическая интерпритация.
Наподобие словарей, где есть название свойства и значение свойства.
Все - обьекты
*/


var obj = {}; // пустой обьект, теоритически может что-то содержать
var person = {
	name : "Arthur", // индентификатор свойства должна быть строка и можно указывать без ковычек
	skill : {
		javascript : 100
		python : 100
	}, // вложеность
	age : 22, // но если испльзовать непонятные символы то только в ковычках
	'' : "Weird"
}; // выражение одного обьекта

person.name;
person.age;
person.skill.javascript;
person.skill.java; // undefined - значение такого нет
// or
person['name']
person['age']
person['']

person.age = 23;
person.skill.ruby = 100;

/* ПРОТОТИПЫ, js основан на прототипах, обьект */
var Human = {
	type : "human",
	head : 1,
	legs : 2
};

var Megahuman = Object.create(Human); // if human прототип megahuman то
Megahuman.type; // "human"
Megahuman.head; // 1

Megahuman.head = 2; // 2
Human.head; // 1

Megahuman.hands = 10;
Megahuman.hands; // 10
Human.hands; // undefined

Human.face; // undefined
Megahuman.face; // undefined

Human.face = 'okay';
Human.face; // 'okay'
Megahuman.face; // 'okay'

Megahuman.face = 'awesome';
Megahuman.face; // 'awesome'
Human.face; // 'okay'

Object.keys(anyObject); // возвращает массив всех ключей anyObject

/* удаление свойств*/
delete Megahuman.head;
Megahuman.head; // 1


/* Massive  +  TIPS*/
massive = [];
massive[massive.length - 1]; // последний елемент массива

massive.push('A'); //  добавление элемента в конец масива
massive.unshift('A1'); // добавление элемента в начале массива

var newVar = massive.pop();// удаляет последний элемент - как в python
var newVar1 = massive.shift(); // аналогично только первый елемент

/* Обьединение массивов */
var mas1 = [1, 2, 3];
var mas2 = [4, 5, 6];
var mas1andmas2 = mas1.concat(mas2);

/* Поиск индекса элемента в массиве */
mas1.indexOf(3); // 2

/* Превращение массива в строку */
mas1.join(); // параметр это разделитель (default: )

/* перебор массива в цикле for */
var animals = ["a", "b", "c"];
for (var i = 0; i < animals.length; i++) {
		console.log(animals[i]);
};

// случайный индекс в масиве ???
var varMassive = [];
Math.floor(Math.random() * varMassive.length)
